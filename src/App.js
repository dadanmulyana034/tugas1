import logo from './logo.svg';
import './App.css';
import { Button } from 'reactstrap';
import { BrowserRouter, Route,Switch } from 'react-router-dom';
import NameForm from './component/form login';
import Daftar from './component/daftar';
import Home from './component/home';


const App = () => {

  return (

    <BrowserRouter>
    <div className="App-logo">
        <Route exact path="/" component={NameForm} />
    </div>
      <div className="App1">
          <Route path="/Daftar" component={Daftar} />
          <Route path="/Home" component={Home} />
      </div>
    </BrowserRouter>
     
  );
}

export default App;
