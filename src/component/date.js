import React from "react";


class Clock extends React.Component {
          constructor(props) {
            super(props);
            this.state = { date: new Date() };
          }
        
        componentDidMount() {
                this.timerID = setInterval(() => this.updateClock(), 1000);
              }
              updateClock() {
                    this.setState({
                      date: new Date()
                    });
                  }
                
       

          render() {
            return (
              <div>
                <h2>NOW AT {this.state.date.toLocaleTimeString()}.</h2>
              </div>
            );
          }

     }   

export default Clock;