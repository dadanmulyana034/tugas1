import React, { useState } from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const Daftar = () => {
    const [namaDepan,setDepan]              = useState('');
    const [namaBelakang, setBelakang]       = useState('');
    const [jenisKelamin, setKelamin]        = useState('dumpling');
    const [tanggal, setTanggal]             = useState('');
    const [email, setEmail]                 = useState('');
    const [password, setPassword]           = useState('');
    const [errorDepan,setErrorDepan]        = useState('');
    const [errorBelakang,setErrorBelakang]  = useState ('');
    const [errorKelamin,setErrorKelamin]    = useState('');
    const [errorTanggal,setErrorTanggal]    = useState ('');
    const [errorEmail,setErrorEmail]        = useState('');
    const [errorPassword,setErrorPassword]  = useState ('');


    const onChangenamaDepan = (e) => {
        const value = e.target.value
        setDepan(value)
        if (!value) {
            setErrorDepan("Nama Tidak Boleh Kosong")
        }
        else {
            setErrorDepan("")
        }
    }
    const onChangenamaBelakang = (e) => {
        const value = e.target.value
        setBelakang(value)
        if (!value) {
            setErrorBelakang("Nama Belakang Tidak Boleh Kosong")
        }
        else {
            setErrorBelakang("")
        }
    }
    const onChangejenisKelamin = (e) => {
        const selectKelamin = e.target.value
        setKelamin(selectKelamin)
        if (!selectKelamin) {
            setErrorKelamin("Pilih Salah Satu")
        }
        else {
            setErrorKelamin("")
        }
    }
    const onChangetanggal = (e) => {
        const value = e.target.value
        setTanggal(value)
        if (!value){
            setErrorTanggal("Isi Tanggal Lahir")
        }
        else {
            setErrorTanggal("")
        }
    }
    const onChangeemail = (e) => {
        const value = e.target.value
        setEmail(value)
        if (!value){
            setErrorEmail("Email Tidak boleh Kosong")
        }
        else {
            setErrorEmail("")
        }
    }
    const onChangepassword = (e) => {
        const value = e.target.value
        setPassword(value)
        if (!value){
            setErrorPassword("passwor Tidak Boleh Kosong")
        }
        else {
            setErrorPassword("")
        }
    }
    return (
        
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="card p-8">  
                            <div className="card-body">
                                <div className="row justify-content-center">
                                <div className="form" style={{fontStyle: "initial"}}>
                                    <br/>
                                <h1>FORM PENDAFTARAN</h1>
                                </div>
                                </div>
                                <h7>*Isi Form Untuk Mendaftar</h7>
                                <br/>
                                <br/>
                                <div className="form-group">
                                    <label>
                                        Nama Depan
                                    </label>
                                    <input type="text" 
                                    placeholder="nama depan" 
                                    className="form-control" 
                                    value={namaDepan} onChange={onChangenamaDepan}/>
                                    {
                                        errorDepan && (
                                            <br className="text-danger"> {errorDepan} </br>
                                        )
                                    }
                                    <label>
                                        Nama Belakang
                                    </label>
                                    <input type="text" 
                                    placeholder="nama belakang" 
                                    className="form-control" 
                                    value={namaBelakang} 
                                    onChange={onChangenamaBelakang}/>
                                    {
                                        errorBelakang && (
                                            <br className="text-danger"> {errorBelakang} </br>
                                        )
                                    }
                                    <label>
                                        Jenis Kelamin
                                    </label>
                                    <br/>
                                    <select>
                                    <option value=""></option>
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                    {jenisKelamin}
                                    </select>
                                    
                                    {
                                        errorKelamin && (
                                            <br className="text-danger"> {errorKelamin} </br>
                                        )
                                    }
                                    <br/>
                                    <label>
                                        Tanggal Lahir
                                    </label>
                                    <input type="date" 
                                    placeholder=" " 
                                    className="form-control" 
                                    value={tanggal} 
                                    onChange={onChangetanggal}/>
                                    {
                                        errorTanggal && (
                                            <br className="text-danger"> {errorTanggal} </br>
                                        )
                                    }
                                    <label>
                                        Email
                                    </label>
                                    <input type="email" 
                                    placeholder="email" 
                                    className="form-control" 
                                    value={email} onChange={onChangeemail}/>
                                    {
                                        errorEmail && (
                                            <br className="text-danger"> {errorEmail} </br>
                                        )
                                    }
                                    <label> 
                                        Password
                                        </label>
                                        <input type="password" 
                                        placeholder="password" 
                                        className="form-control" 
                                        value={password} 
                                        onChange={onChangepassword}/>
                                        {
                                            errorPassword && (
                                                <br className="text-danger"> {errorPassword} </br>
                                            )
                                        }
                                </div>
                                <br/>
                                <div className="row">
                                    <div className="col-md-6">
                                        <Link to="/">
                                        <Button className="btn btn-primary">Submit</Button> 
                                        </Link>
                                    </div>

                                    <div className="col-md-6 text-right">
                                    <Link to="/" className>
                                        Sudah Punya Akun?
                                    </Link>  
                                    </div>
                                </div>   
                                
                            </div>
                        </div>
    
                    </div>
                </div>
            </div>
        
   
    );
}

export default Daftar
