import React from 'react';
import { Link } from 'react-router-dom';
import Avatar from './avatar';


class NameForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {value1: '', value2: ''};

      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleChange2 = this.handleChange2.bind(this);
      
    }
  
    handleChange(event) {    
        this.setState({value1: event.target.value});  
    }
    handleChange2(event) {
        this.setState({value2: event.target.value});  
    }
    handleSubmit(event) {
        
            alert('ANDA TELAH LOGIN');
            
        event.preventDefault(); }
    render() {
      return (
        
        <div style={{marginTop: "50px"}}>
            <div className="container">
            <div className="conten-center">
            <Avatar avatar='https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS8VK8S-A7kZpno7GH9I7-HpOriFtvd42h0WQ&usqp=CAU'/>
            </div>
                <div className="row justify-content-center">
                    <div className="col-md-4">
                        <div className="card p-2">  
                            <div className="card-body">
                                <div className="form-group">
                                <h1>LOGIN</h1>
                                    <form onSubmit={this.handleSubmit}>
                                    <label>
                                        Userame
                                    </label>
                                    <input type="email" 
                                    value={this.state.value} 
                                    onChange={this.handleChange} 
                                    className="form-control" />
                                    <label>
                                        Password
                                    </label> 
                                        <input type="password" 
                                        value={this.state.value} 
                                        onChange={this.handleChange2} 
                                        className="form-control" />
                                    <br/>
                                    <div className="row">
                                        <div className="col-md-4">
                                        <Link to="/Home">    
                                        <input type="submit" 
                                        value="LogIn" 
                                        style={{color: "black"},{backgroundColor: "lightBlue"}}/>
                                        </Link>
                                        </div>
                                    
                                        <div className="col-md-7">
                                            <Link to="/daftar" >
                                           Belum Punya Akun?
                                            </Link>
                                        </div>   
                                     </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
              );
    }
}

export default NameForm
